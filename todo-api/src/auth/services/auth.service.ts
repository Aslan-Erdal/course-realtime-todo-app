import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { IUser } from "src/user/user.interfaces";
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {

    constructor(private jwtService: JwtService) {}

    async generateJwt(user: IUser): Promise<string> {
        return this.jwtService.signAsync({user})
    }

    async hashPassword(password: string): Promise<string> { 
        return bcrypt.hash(password, 12);
    }

    async comparePasswords(password: string, storedPAsswordHash: string): Promise<any> {
        return bcrypt.compare(password, storedPAsswordHash);
     }

     verifyJwt(jwt: string): Promise<any> { 
        return this.jwtService.verifyAsync(jwt);
     }
}