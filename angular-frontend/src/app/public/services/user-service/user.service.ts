import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { ILoginResponse, IUser } from '../../public.interfaces';
import { Observable, catchError, tap, throwError } from 'rxjs';
import { LOCALSTORAGE_KEY_NESTJS_TODO_APP } from 'src/app/app.module';
import { JwtHelperService } from '@auth0/angular-jwt';

export const snackBarConfig: MatSnackBarConfig = {
  duration: 2500,
  horizontalPosition: 'right',
  verticalPosition: 'top'
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private httpClient: HttpClient,
    private snackbar: MatSnackBar,
    private jwtService: JwtHelperService,
    ) { }

    public login(user: IUser): Observable<ILoginResponse> { 
      return this.httpClient.post<ILoginResponse>('api/users/login', user).pipe(
        tap((res: ILoginResponse) => localStorage.setItem(LOCALSTORAGE_KEY_NESTJS_TODO_APP, res.access_token)),
        tap(() => this.snackbar.open('Login Successfull', 'Close', snackBarConfig)),
        catchError(e => {
          this.snackbar.open(`${e.error.message}`, 'Close', snackBarConfig);
          return throwError(() => new Error(e));
        })
      )
    }

    public register(user: IUser): Observable<IUser>{
      return this.httpClient.post<IUser>('api/users', user).pipe(
        tap((createdUser: IUser) => this.snackbar.open(`User ${createdUser.username} was created`, 'Close', snackBarConfig)),
        catchError(e => {
          this.snackbar.open(`User could not be created because: ${e.error.message}`, 'Close', snackBarConfig);
          return throwError(() => new Error(e));
        })
      )
     }

     public getLoginUser() {
      const decodedToken = this.jwtService.decodeToken();
      return decodedToken.user;
     }
}
