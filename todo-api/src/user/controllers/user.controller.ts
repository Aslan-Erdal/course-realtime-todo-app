import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { UserService } from '../services/user.service';
import { DtoHelperService } from '../dto/dto-helper.service';
import { ILoginResponse, IUser } from '../user.interfaces';
import { CreateUserDto } from '../dto/create-user.dto';
import { LoginUserDto } from '../dto/login-user.dto';

@Controller('users')
export class UserController {

  constructor(
    private readonly userService: UserService, 
    private dtoHelperService: DtoHelperService
    ) {}

    @Post()
    async create(@Body() createUserDto: CreateUserDto): Promise<IUser> {
      const userEntity: IUser = await this.dtoHelperService.createUserDtoToEntity(createUserDto);
      return this.userService.create(userEntity);
    }

    @Post('login')
    async login(@Body() loginUserDto: LoginUserDto): Promise<ILoginResponse> {
      const userEntity: IUser = await this.dtoHelperService.loginUserDtoToEntity(loginUserDto);
      const jwt: string = await this.userService.login(userEntity);
      return {
        access_token: jwt,
        token_type: 'JWT',
        expires_in: 10000
      }
     }


}
