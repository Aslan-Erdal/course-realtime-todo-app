import { UnauthorizedException } from '@nestjs/common';
import { OnGatewayConnection, OnGatewayDisconnect, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { AuthService } from 'src/auth/services/auth.service';
import { UserService } from 'src/user/services/user.service';
import { IUser } from 'src/user/user.interfaces';
import { ConnectionService } from '../services/connection.service';
import { TodoService } from '../services/todo.service';

@WebSocketGateway({namespace: 'todos', cors: { origin: ['http://localhost:3000', 'http://localhost:4200']}})
export class TodoGateway implements OnGatewayConnection, OnGatewayDisconnect {

  @WebSocketServer()
  server: Server;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private connectionService: ConnectionService,
    private todoService: TodoService
    ) { }

  async handleConnection(socket: Socket) {
    try {
      // if the token is not verified, this will throw and we can catch & disconnect the user
      console.log(socket.handshake.auth.Authorization);// see is token is getting or not
      
      const decodedToken = await this.authService.verifyJwt(socket.handshake.auth.Authorization);
      // if the token is valid, we get the user by id from our database
      const user: IUser = await this.userService.getOneById(decodedToken.user.id);

      if (!user) {
        console.log('disconnect user');
        return this.disconnect(socket);
      } else {
        console.log('do something', user);

        // save the connection of the user in our database;
        await this.connectionService.create({ socketId: socket.id, connectedUser: user })

        // get all todos from the database
        const todos = await this.todoService.findAll();

        // Only emit todos to specific connected client
        return this.server.to(socket.id).emit('todos', todos);

      }
    } catch {
      console.log('Disconnect user');
      return this.disconnect(socket);
    }
  }

 async handleDisconnect(socket: Socket) {
    // remove the connection from our db;
    await this.connectionService.deleteBySocketId(socket.id);
    socket.disconnect();
  }

  private disconnect(socket: Socket) {
    socket.emit('Error', new UnauthorizedException());
    socket.disconnect();
  }
}
