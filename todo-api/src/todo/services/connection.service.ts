import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection } from '../entities/connection.entity';
import { Repository } from 'typeorm';
import { IConnection } from '../todo.interface';

@Injectable()
export class ConnectionService {
    constructor(
        @InjectRepository(Connection)
        private readonly connectionRepository: Repository<Connection>
    ) { }

    async create(connection: IConnection): Promise<IConnection> {
        return this.connectionRepository.save(connection);
    }

    async findByuserId(userId: number): Promise<Connection[]> {
        return this.connectionRepository.find({
            where: {
                connectedUser: {
                    id: userId
                }
            }
        })
    }

    async deleteBySocketId(socketId: string) {
        return this.connectionRepository.delete({socketId});
    }

    async deleteAll() {
        await this.connectionRepository.createQueryBuilder().delete().execute();
    }
}
