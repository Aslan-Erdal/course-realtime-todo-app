import { IUser } from "src/user/user.interfaces";

export type Status = 'BACKLOG' | 'TODO' | 'DONE';
export type Complexity = 'EASY' | 'MEDIUM' | 'HARD';

export interface TodoItem {
    id?: number,
    createdBy?: IUser;
    updatedBy?: IUser;
    createdAt?: Date;
    updateAt?: Date;

    status: Status;
    title: string;
    subTitle: string;
    text: string;
    complexity: Complexity;
}

export interface IConnection {
    id?: number;
    socketId?: string;
    connectedUser?: IUser;
}