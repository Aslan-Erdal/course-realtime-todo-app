import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { Complexity, Status } from "../todo.interface";
// import { IUser } from "src/user/user.interfaces";

@Entity()
export class Todo {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    status: Status;

    @Column()
    title: string;

    @Column()
    subTitle: string;

    @Column()
    text: string;

    @Column()
    complexity: Complexity;

    // createdBy?: IUser;
    // updatedBy?: IUser;
    // createdAt?: Date;
    // updatedAt?: Date;

}